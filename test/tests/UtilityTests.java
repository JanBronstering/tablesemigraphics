package tests;

import org.junit.Assert;
import org.junit.Test;
import static org.junit.Assert.*;
import tablesemigraphics.Utilities;
import static tablesemigraphics.Utilities.evaluateGeometryOfStringArray;

/**
 *
 * @author jan bronstering
 */
public class UtilityTests
{
    
    @Test
    public void evaluateGeometryOfStringArrayTest()
    {
        String[] stringArray = {"a", "ab", "abc", "abcd", "abcde"};
        int[] intArrayExpected = {1,2,3,4,5};

        int[] intArrayReceived = evaluateGeometryOfStringArray(stringArray);

        assertArrayEquals(intArrayExpected, intArrayReceived);

        // Check String with one element.

        stringArray = new String[] {"a % 5 @ €"};
        intArrayExpected = new int[] {9};

        intArrayReceived = evaluateGeometryOfStringArray(stringArray);

        assertArrayEquals(intArrayExpected, intArrayReceived);

        // Check array with no elements.

        stringArray = new String[0];
        intArrayExpected = new int[0];

        intArrayReceived = evaluateGeometryOfStringArray(stringArray);

        assertArrayEquals(intArrayExpected, intArrayReceived);
    }
	
    @Test
    public void centerTest()
    {
        String s = "Kreatur";
        String expected = " Kreatur  ";
        assertEquals(expected, Utilities.center(s, 10));

        s = "Bundesliga";
        expected = "  Bundesliga   ";
        assertEquals(expected, Utilities.center(s, 15));

        expected = "   Bundesliga   ";
        assertEquals(expected, Utilities.center(s, 16));

        expected = "desl";
        assertEquals(expected, Utilities.center(s, 4));

        s = "Simon's Cat";
        expected = "on's ";
        assertEquals(expected, Utilities.center(s, 5));

        expected = "";
        assertEquals(expected, Utilities.center(s, 0));

    }

    @Test
    public void centerSBTest()
    {
        StringBuilder sb = new StringBuilder();

        String s = "Kreatur";
        String expected = " Kreatur  ";
        Utilities.center(sb, s, 10);
        String actual = sb.toString();
        assertEquals(expected, actual);

        sb = new StringBuilder();
        s = "Bundesliga";
        expected = "  Bundesliga   ";
        Utilities.center(sb, s, 15);
        actual = sb.toString();
        assertEquals(expected, actual);

        sb = new StringBuilder();
        s = "Bundesliga";
        expected = "   Bundesliga   ";
        Utilities.center(sb, s, 16);
        actual = sb.toString();
        assertEquals(expected, actual);

        sb = new StringBuilder();
        s = "Simon's Cat";
        expected = s;
        Utilities.center(sb, s, 5);
        actual = sb.toString();
        assertEquals(expected, actual);

        sb = new StringBuilder();
        s = "Simon's Cat";
        expected = s;
        Utilities.center(sb, s, 0);
        actual = sb.toString();
        assertEquals(expected, actual);	
    }

    @Test
    public void maxValueArrayTest()
    {
        int[] arrayA = new int[] { 59, 13, 87, 22,104, 12, 51, 49};
        int[] arrayB = new int[] {100, 90, 66, 50,  2,  9,106, 55};
        int[] arrayC = new int[] { 17, 42, 14, 67, 78, 27, 43,107};
        int[] arrayD = new int[] {-33,101, 23, 49, 69,105, 92, 34};
        int[] arrayE = new int[] {  2, -2,102,103,-33, 98,  5, 19};
        
        int[] maximums = Utilities.maxValueArray(arrayA, arrayB, arrayC, arrayD, arrayE);
        int[] expected = new int[] {100,101,102,103,104,105,106,107};
        
        Assert.assertArrayEquals(maximums,expected);
    }
    
    @Test
    public void minValueArrayTest()
    {
        int[] arrayA = new int[] {  59,  13,  87,  22, -96,  12,  51,  49};
        int[] arrayB = new int[] {-100,  90,  66,  50,   2,   9, -94,  55};
        int[] arrayC = new int[] {  17,  42,  14,  67,  78,  27,  43, -93};
        int[] arrayD = new int[] { -33, -99,  23,  49,  69, -95,  92,  34};
        int[] arrayE = new int[] {   2,  -2, -98, -97, -33,  98,   5,  19};
        
        int[] minimums = Utilities.minValueArray(arrayA, arrayB, arrayC, arrayD, arrayE);
        int[] expected = new int[] {-100,-99,-98,-97,-96,-95,-94,-93};
        
        Assert.assertArrayEquals(minimums,expected);
    }
    
}
