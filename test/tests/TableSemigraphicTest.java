package tests;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tablesemigraphics.TableSemigraphic;

/**
 *
 * @author jan bronstering
 */
public class TableSemigraphicTest
{
    TableSemigraphic tsg;
    
    String[][] seasons;
    
    public TableSemigraphicTest()
    {
        this.seasons = new String[][]{
            { "Januar", "Februar", "März", "April",},
            { "Mai", "Juni", "Juli", "August",},
            { "September", "Okt", "Nov", "Dez"}
        };
    }
    
    @BeforeClass
    public static void setUpClass()
    {
    }
    
    @AfterClass
    public static void tearDownClass()
    {
    }
    
    @Before
    public void setUp()
    {
        tsg = new TableSemigraphic(seasons);
    }
    
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void testDefaultPresentation()
    {
        
        String expected = "┏━━━━━━━━━┳━━━━━━━┳━━━━┳━━━━━━┓\n"
                        + "┃Januar   ┃Februar┃März┃April ┃\n"
                        + "┣━━━━━━━━━╋━━━━━━━╋━━━━╋━━━━━━┫\n"
                        + "┃Mai      ┃Juni   ┃Juli┃August┃\n"
                        + "┣━━━━━━━━━╋━━━━━━━╋━━━━╋━━━━━━┫\n"
                        + "┃September┃Okt    ┃Nov ┃Dez   ┃\n"
                        + "┗━━━━━━━━━┻━━━━━━━┻━━━━┻━━━━━━┛\n";

        assertEquals(expected, tsg.toString());
    }
    
    @Test 
    public void testWithoutSeparators()
    {
        tsg.drawSeparatorsBetweenRows(false);
                
        String expected = "┏━━━━━━━━━┳━━━━━━━┳━━━━┳━━━━━━┓\n"
                        + "┃Januar   ┃Februar┃März┃April ┃\n"
                        + "┃Mai      ┃Juni   ┃Juli┃August┃\n"
                        + "┃September┃Okt    ┃Nov ┃Dez   ┃\n"
                        + "┗━━━━━━━━━┻━━━━━━━┻━━━━┻━━━━━━┛\n";

        assertEquals(expected, tsg.toString());
    }
    
    @Test
    public void testHeadline()
    {
        tsg.setHeadline("Jahreszeiten");
        
        String expected = "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n"
                        + "┃        Jahreszeiten         ┃\n"
                        + "┣━━━━━━━━━┳━━━━━━━┳━━━━┳━━━━━━┫\n"
                        + "┃Januar   ┃Februar┃März┃April ┃\n"
                        + "┣━━━━━━━━━╋━━━━━━━╋━━━━╋━━━━━━┫\n"
                        + "┃Mai      ┃Juni   ┃Juli┃August┃\n"
                        + "┣━━━━━━━━━╋━━━━━━━╋━━━━╋━━━━━━┫\n"
                        + "┃September┃Okt    ┃Nov ┃Dez   ┃\n"
                        + "┗━━━━━━━━━┻━━━━━━━┻━━━━┻━━━━━━┛\n";
        
        assertEquals(expected, tsg.toString());                
    }
    
    @Test
    public void testHeadlineWithoutSeparators()
    {
        tsg.setHeadline("Jahreszeiten");
        
        tsg.drawSeparatorsBetweenRows(false);
        
        String expected = "┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓\n"
                        + "┃        Jahreszeiten         ┃\n"
                        + "┣━━━━━━━━━┳━━━━━━━┳━━━━┳━━━━━━┫\n"
                        + "┃Januar   ┃Februar┃März┃April ┃\n"
                        + "┃Mai      ┃Juni   ┃Juli┃August┃\n"
                        + "┃September┃Okt    ┃Nov ┃Dez   ┃\n"
                        + "┗━━━━━━━━━┻━━━━━━━┻━━━━┻━━━━━━┛\n";
        
        assertEquals(expected, tsg.toString());    
    }
    
    
}
