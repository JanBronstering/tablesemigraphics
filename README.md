# README #

TableSemigraphics is a small Java-project to visualize data in table form with box-drawing unicode characters:





```
#!terminal

┏━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┓
┃                        This is useful...                         ┃
┣━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┳━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┫
┃...to present data on terminal.┃...to include tables in textfiles.┃
┗━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┻━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━┛
```




# This Example #

```
#!java


String one = "This is useful...";
String two = "...to present data on terminal.";
String three = "...to include tables in textfiles.";
String[][] captions = new String[][] {{two,three}};

TableSemigraphic tsg = new TableSemigraphic(captions);
tsg.setHeadline(one);
System.out.println(tsg.toString());
```