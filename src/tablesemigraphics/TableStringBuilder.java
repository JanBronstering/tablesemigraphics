package tablesemigraphics;

/**
 * A TableStringBuilder is a StringBuilder to create a String representation of a table row by row.
 * @author jan bronstering
 * @version 0.05
 */
public class TableStringBuilder
{
    private StringBuilder sb;
    private int[] geometry;
    private char[] alignment;
    private char[] drawingElements;
    private int numberOfCells;
    private int tableWidth;
    private int headlineWidth;

    /**
     * Constructor.
     * Constructs a new TableStringBuilder based on a passed StringBuilder.
     * @param sb                The StringBuilder to be extended with table representation.
     * @param geometry            An int array holding the cell-widths. 
     * @param alignment            A char array holding the cell alignments (chars: l,c,r).
     * @param drawingElements    A char array which holds the unicode elements to draw a table.
     */
    public TableStringBuilder(StringBuilder sb, int[] geometry, char[] alignment, char[] drawingElements)
    {
        if(geometry.length != alignment.length)
            throw new IllegalArgumentException("Array geometry and array alignment must be of the same size.");
        
        if(Utilities.minValue(geometry) < 1)
            throw new IllegalArgumentException("Invalid geometry: cell-widths must be 1 at least.");
        
        this.numberOfCells = geometry.length;
        
        this.sb = sb;
        this.geometry = geometry;
        this.alignment = alignment;
        this.drawingElements = drawingElements;
        this.tableWidth = 1 +  Utilities.sumUpArray(geometry) + numberOfCells;
        this.headlineWidth = tableWidth - 2;
    }
    
    /**
     * Constructor.
     * Constructs a new TableStringBuilder based on a new StringBuilder.
     * @param geometry            An int array holding the cell-widths. 
     * @param alignment            A char array holding the cell alignments (chars: l,c,r).
     * @param drawingElements    A char array which holds the unicode elements to draw a table.
     */
    public TableStringBuilder(int[] geometry, char[] alignment, char[] drawingElements)
    {
        this(new StringBuilder(), geometry, alignment, drawingElements);    
    }
    
    /**
     * Constructor.
     * Constructs a new TableStringBuilder based on a new StringBuilder.
     * The instance will use the default drawing elements.
     * @param geometry            An int array holding the cell-widths. 
     * @param alignment            A char array holding the cell alignments (chars: l,c,r).
     */
    public TableStringBuilder(int[] geometry, char[] alignment)
    {
        this(new StringBuilder(), geometry, alignment, getDefaultDrawingElements());
    }
    
    /**
     * Constructor.
     * Constructs a new TableStringBuilder based on a passed StringBuilder.
     * The instance will use the default drawing elements.
     * @param sb                The StringBuilder to be extended with table representation.
     * @param geometry            An int array holding the cell-widths. 
     * @param alignment            A char array holding the cell alignments (chars: l,c,r).
     */
    public TableStringBuilder(StringBuilder sb, int[] geometry, char[] alignment)
    {
        this(sb, geometry, alignment, getDefaultDrawingElements());
    }

    /**
     * Draws a top table border, like ┏━━━━━┳━━┳━━━┳━━┳━━━┓.
     */
    public void drawTop()
    {
        // ┏ ┳ ┓ ━
        drawLine(drawingElements[0], drawingElements[6], drawingElements[1], drawingElements[10]);
    }

    /**
     * Draws a top table border without cell stubs, like ┏━━━━━━━━━━━━━━━━━━━┓.
     */
    public void drawTopWithoutStubs()
    {
        // ┏ ━ ┓ ━
        drawLine(drawingElements[0], drawingElements[10], drawingElements[1], drawingElements[10]);
    }

    /**
     * Draws a table line between two rows, like ┣━━━━━╋━━╋━━━╋━━╋━━━┫.
     */
    public void drawSeparator()
    {
        // ┣ ╋ ┫ ━
        drawLine(drawingElements[5], drawingElements[8], drawingElements[7], drawingElements[10]);        
    }

    /**
     * Draws a table line between two rows without cell stubs, like ┣━━━━━━━━━━━━━━━━━━━┫.
     */
    public void drawSeparatorWithoutStubs()
    {
        // ┣ ━ ┫ ━
        drawLine(drawingElements[5], drawingElements[10], drawingElements[7], drawingElements[10]);    
    }
    
    
    /**
     * Draws a table line between two rows without upper cell stubs, like ┣━━━━━┳━━┳━━━┳━━┳━━━┫.
     */
    public void drawSeparatorWithoutUpperStubs()
    {
        // ┣ ┳ ┫ ━
        drawLine(drawingElements[5], drawingElements[6], drawingElements[7], drawingElements[10]);    
    }

    /**
     * Draws a table line between two rows without lower cell stubs, like ┣━━━━━┻━━┻━━━┻━━┻━━━┫.
     */
    public void drawSeparatorWithoutLowerStubs()
    {
        // ┣ ┻ ┫ ━
        drawLine(drawingElements[5], drawingElements[4], drawingElements[7], drawingElements[10]);    
    }
    

    /**
     * Draws a bottom table border, like ┗━━━━━┻━━┻━━━┻━━┻━━━┛.
     */
    public void drawBottom()
    {
        // ┗ ┻ ┛ ━
        drawLine(drawingElements[3], drawingElements[4], drawingElements[2], drawingElements[10]);
    }
    
    /**
     * Draws a bottom table border without cell stubs, like ┗━━━━━━━━━━━━━━━━━━━┛.
     */
    public void drawBottomWithoutStubs()
    {
        // ┗ ━ ┛ ━
        drawLine(drawingElements[3], drawingElements[10], drawingElements[2], drawingElements[10]);
    }
    
    /**
     * Draws a line between the table contents.
     * @param leftBorder        char which represents the left border of the line.
     * @param midSeparator      char which represents the separator stump between two cells.
     * @param rightBorder       char which represents the right border of the line.
     * @param horizontalLine    char which represents part of the horizontal line.
     */
    private void drawLine(char leftBorder, char midSeparator, char rightBorder, char horizontalLine )
    {
        int lastCellIndex = numberOfCells-1;
        
        sb.append(leftBorder);
        
        for(int i = 0; i < numberOfCells; i++)
        {
            Utilities.repeatChar(sb, horizontalLine, geometry[i]);
            
            if(i < lastCellIndex)
                sb.append(midSeparator);
        }
        
        sb.append(rightBorder);
        
        sb.append("\n");
    }

    /**
     * Draws a table row based on the cell captions and an alignment-char for each cell.
     * @param captions    cell captions
     * @param alignment    cell alignments
     * TODO: To be refactored. Accentuate the approach with String.format().
     */
    public void drawRow(String[] captions, char[] alignment)
    {
        StringBuilder fsb = new StringBuilder();
        
        // A little hack to keep the original captions unchanged if center-alignment is required.
        if(Utilities.containsLetter(alignment, 'c'))
        {
            String[] captionsCopy = new String[captions.length];
            System.arraycopy(captions, 0, captionsCopy, 0, captions.length);
            captions = captionsCopy;
        }
        
        fsb.append(drawingElements[9]);                                                // left border
        
        // format per cell example (cell width 7, left align):        123456
        //                                                            %-7.7s
        
        for(int i = 0; i < numberOfCells; i++)
        {
            String geoString = Integer.toString(geometry[i]);
            
            fsb.append('%');                                                        // (1)
            
            if(Utilities.isLetter(alignment[i], 'C'))
                captions[i]=Utilities.center(captions[i], geometry[i]);
            else if(Utilities.isLetter(alignment[i], 'L'))
                fsb.append('-');                                                    // (2)
            
            fsb.append(geoString);                                                  // (3)
            
            fsb.append('.');                                                        // (4)
            
            fsb.append(geoString);                                                  // (5)
            
            if(Character.isLowerCase(alignment[i]))                                 // (6)
                fsb.append('s');
            else
                fsb.append('S');
            
            fsb.append(drawingElements[9]);                                         // right border
        }
        
        sb.append(String.format(fsb.toString(), (Object[])captions));
        sb.append("\n");        
    }
    
    /**
     * Draws a table row based on the cell captions with adjusted alignment.
     * @param captions    cell captions
     */
    public void drawRow(String[] captions)
    {
        drawRow(captions, this.alignment);
    }

    /**
     * Draws a headline.
     * @param headline    The headline.
     */
    public void drawHeadline(String headline)
    {
        sb.append(drawingElements[9]);
        Utilities.center(sb, headline, headlineWidth);
        sb.append(drawingElements[9]);
        sb.append("\n");
    }
    
    /**
     * Sets the unicode elements for table drawing.
     * @param drawingElemets
     */
    public void setDrawingElements(char[] drawingElemets)
    {
        this.drawingElements = drawingElemets;
    }
    
    /**
     * Sets an default drawing element set.
     */
    public void setDrawingElements()
    {
        drawingElements = getDefaultDrawingElements();
    }
    
    /**
     * Generates a default drawing element set.
     * @return the drawing element set.
     */
    private static char[] getDefaultDrawingElements()
    {
        char[] drawingElements = new char[11];
        drawingElements[0]  = '┏';
        drawingElements[1]  = '┓';
        drawingElements[2]  = '┛';
        drawingElements[3]  = '┗';
        drawingElements[4]  = '┻';
        drawingElements[5]  = '┣';
        drawingElements[6]  = '┳';
        drawingElements[7]  =  '┫';
        drawingElements[8]  = '╋';
        drawingElements[9]  = '┃';
        drawingElements[10] = '━';
        return drawingElements;
    }
    
    /**
     * Returns the StringBuilder.
     * @return StringBuilder with table data.
     */
    public StringBuilder getStringBuilder()
    {
        return sb;
    }
    
    /**
     * Clears the TableStringBuffer.
     */
    public void clear()
    {
        sb.setLength(0);
    }
    
    /**
     * Creates the string representation of the table from StringBuilder.
     * Note that the StringBuilder possibly was turned over with other data besides the table.
     * @return 
     */
    @Override
    public String toString()
    {
        return sb.toString(); 
    }

    void addHeadlineOrTopborder(TableStringBuilder tsb)
    {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}

