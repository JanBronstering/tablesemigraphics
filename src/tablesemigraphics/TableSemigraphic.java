package tablesemigraphics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

/**
 * This class encapsulates an AbstractTable and provides all needed data for a visual table representation on terminal.
 * @author jan bronstering
 * @version 0.05
 *
 */
public class TableSemigraphic
{    
    private final int columnCount;
    private final int rowCount;
    TableStringBuilder tableStringBuilder;
    
    private final List<String[]> rows;
    
    private int[] columnWidths;
    
    private String headline;
    private boolean headlineActive;
    
    private boolean drawSeparatorsBetweenRows;
    
    private String stringRepresentation;
    private final char[] columnAlignment;

    /**
     * Constructor. Creates a new TableSemigraphic an on top a 2d-array holding
     * the content.
     * @param content The cell-captions row by row and column by column.
     */
    public TableSemigraphic(String[][] content)
    {
        rows = Arrays.asList(content);
        this.columnCount = rows.get(0).length; // todo: check if array is symmetric
        this.rowCount = rows.size();
        
        this.columnAlignment = new char[columnCount];
        this.columnWidths = new int[columnCount];
        this.drawSeparatorsBetweenRows = true;
        
        setColumnWidthsFittingly();
        setDefaultAlignments();
    }
    
    /**
     * Sets the default alignments for columns.
     * todo: Alignments for headline and first row.
     */
    private void setDefaultAlignments()
    {
        Arrays.fill(columnAlignment, 'l');
    }
    
    /**
     * Sets the columnWidths so that every cell caption is displayed entirely.
     */
    private void setColumnWidthsFittingly()
    {
        List<int[]> stringLengthRows = new ArrayList<>();
        int[][] stringLengthRowsAsArray;
        
        rows.forEach(row -> stringLengthRows.add(Utilities.evaluateGeometryOfStringArray(row)));
        
        stringLengthRowsAsArray = new int[stringLengthRows.size()][stringLengthRows.get(0).length];
        
        stringLengthRows.toArray(stringLengthRowsAsArray);
        
        columnWidths = Utilities.maxValueArray(stringLengthRowsAsArray);
    }
    
    /**
     * Refreshes the string representation to match the current table-content
     * and formatts.
     */
    private void refreshStringRepresentation()
    {
        TableStringBuilder tsb = new TableStringBuilder(columnWidths, columnAlignment);
        
        addHeadlineOrTopborder(tsb);
        addRows(tsb);
        
        stringRepresentation = tsb.toString();
    }
    
    /**
     * Helpermethod for refreshStringRepresentation().
     * If the headline is activated the headlinesegment is drawn.
     * If the headline is deactivated the upper border is drawn.
     * @param tsb TableStringBuilder to be supplemented.
     */
    private void addHeadlineOrTopborder(TableStringBuilder tsb)
    {
        if (headlineActive)
        {
            tsb.drawTopWithoutStubs();
            tsb.drawHeadline(headline);
            tsb.drawSeparatorWithoutUpperStubs();
        }
        else
        {
            tsb.drawTop();
        }
    }
    
    /**
     * Helpermethod for refreshStringRepresentation().
     * Add row by row 
     * @param tsb 
     */
    private void addRows(TableStringBuilder tsb)
    {
        Iterator<String[]> rowsIterator = rows.iterator();
        
        while(rowsIterator.hasNext())
        {
            tsb.drawRow(rowsIterator.next());
            
            boolean lastRow = !rowsIterator.hasNext();
            
            if(lastRow)
                tsb.drawBottom();
            else if(drawSeparatorsBetweenRows)
                tsb.drawSeparator();
        }
    }
    
    @Override
    public String toString()
    {
        refreshStringRepresentation();
        return stringRepresentation;
    }

    public void setHeadline(String headline) {
        this.headline = headline;
        this.headlineActive = true;
    }

    public void removeHeadline() {
        this.headlineActive = false;        
    }

    public void drawSeparatorsBetweenRows(boolean b)
    {
        drawSeparatorsBetweenRows = b;
    }    
}    
