package tablesemigraphics;

import java.util.Arrays;
import java.util.Iterator;

/**
 * Several utility methods accumulated while working on BallTable-Project.
 * 
 * @author jan bronstering
 * @version 0.05
 *
 */
public class Utilities
{
    // todo: Consider refacoring with stream-features for the following 4 methods.
    
    /**
     * Compares consecutive pairs of elements of two given arrays and returns an array
     * of the biggest values. The two arrays must be the same length. 
     * @param a Array a to compare.
     * @param b Array b to compare.
     * @return The array with the bigger values.
     */
    public static int[] maxValueArray(int[] a, int[] b)
    {
        if(a.length != b.length) 
            throw new IllegalArgumentException("Both arrays must be of the same length.");
        int[] m = new int[a.length];
        for(int i=0; i < a.length; i++){
            m[i] = Math.max(a[i], b[i]); 
        }
        return m;
    }
    
    /**
     * Compares consecutive pairs of elements of n given arrays and returns an array
     * of the biggest values. The two arrays must be the same length. 
     * @param arrays Arrays to be compared.
     * @return 
     */
    public static int[] maxValueArray(int[]... arrays)
    {
        int[] result;
        
        if(arrays.length < 1)
            throw new IllegalArgumentException("At least 1 array is mandatory.");
        
        Iterator<int[]> it = Arrays.asList(arrays).iterator();
        result = it.next();
        
        while(it.hasNext())
        {
            result = maxValueArray(result, it.next());
        }
        
        return result;
    }
    
    /**
     * Compares consecutive pairs of elements of two given arrays and returns an array
     * of the smallest values. The two arrays must be the same length. 
     * @param a Array a to compare.
     * @param b Array b to compare.
     * @return The array with the smaller values.
     */
    public static int[] minValueArray(int[] a, int[] b)
    {
        if(a.length != b.length) 
            throw new IllegalArgumentException("Both arrays must be of the same length.");
        int[] m = new int[a.length];
        for(int i=0; i < a.length; i++){
            m[i] = Math.min(a[i], b[i]); 
        }
        return m;
    }
    
    /**
     * Compares consecutive pairs of elements of n given arrays and returns an array
     * of the smallest values. The two arrays must be the same length. 
     * @param arrays Arrays to be compared.
     * @return The array with the smaller values.
     */
    public static int[] minValueArray(int[]... arrays)
    {
        int[] result;
        
        if(arrays.length < 1)
            throw new IllegalArgumentException("At least 1 array is mandatory.");
        
        Iterator<int[]> it = Arrays.asList(arrays).iterator();
        result = it.next();
        
        while(it.hasNext())
        {
            result = minValueArray(result, it.next());
        }
        
        return result;
    }
    
    /**
     * Returns the maximum of an int-Array.
     * @param array
     * @return The maximum (largest value) of the given array.
     */
    public static int maxValue(int[] array)
    {
        return Arrays.stream(array).min().getAsInt();
    }
    
    /**
     * Returns the minimum of an int-Array.
     * @param array
     * @return The minimum (smallest value) of the given array.
     */
    public static int minValue(int[] array)
    {
        return Arrays.stream(array).min().getAsInt();
    }
    
    /**
     * Sums up the numbers in a given array.
     * @param     array    The array of the number to sum up.
     * @return    The sum of the array-numbers.
     */
    public static int sumUpArray(int[] array)
    {
        int result = 0;
        
        for(int i: array)
            result += i;
        
        return result;
    }

    /**
     * Prints an overview of an Array by printing every element.
     * @param array The array to be printed.
     */
    public static void printArray(Object[] array)
    {
        int count = 0;
        for(Object i: array)
        {
            System.out.println("[" + count + "] -> " + i);
            count++;
        }
        System.out.println();
    }
    
    /**
     * Converts an int[]-arrays to an Intger[]-array.
     * @param primitivesArray A int[]-array.
     * @return The corresponding Integer[]-array.
     */
    public static Integer[] arrayInt2Integer(int[] primitivesArray)
    {
        Integer[] wrapperArray = new Integer[primitivesArray.length];
        int count = 0;
        for(int i: primitivesArray)
        {
            wrapperArray[count] = i;
            count++;
        }
        return wrapperArray;        
    }
    
    // String and Stringarrays
    
    /**
     * Takes a StringBuilder and appends a char severel times to it.
     * @param     sb        The StringBuilder to which the char is to be added.
     * @param     c        The char.
     * @param     repeat    The number of times the char is to be added.
     */
    public static void repeatChar(StringBuilder sb, char c, int repeat)
    {
        for(int i = 1; i <= repeat; i++)
        {
            sb.append(c);
        }
    }
    
    // TODO adopt width < length behavior
    public static void center(StringBuilder sb, String s, int width)
    {
        int l = s.length();
        
        int freeSpace = width-l;
        
        if(freeSpace <= 0)
        {
            sb.append(s);
            return;
        }
        
        int leftSpace = freeSpace/2;
        int rightSpace = freeSpace-leftSpace;
        
        repeatChar(sb, ' ', leftSpace);
        sb.append(s);
        repeatChar(sb, ' ', rightSpace);
    }
    
    // TODO decide implement the exact behaviour if width < stringlength
    public static String center(String s, int width)
    {
        int l = s.length();
        
        int freeSpace = width-l;
        int freeSpaceAbs = Math.abs(freeSpace);
        int leftSpace = freeSpaceAbs/2;
        int rightSpace = freeSpaceAbs-leftSpace;
        
        if(freeSpace == 0)
        {
            return s;
        }
        else if(freeSpace > 0)
        {
            StringBuilder sb = new StringBuilder(width);
            repeatChar(sb, ' ', leftSpace);
            sb.append(s);
            repeatChar(sb, ' ', rightSpace);
            return sb.toString();
        } 
        else 
        {
            return s.substring(leftSpace, l-rightSpace);
        }
            
    }
    
    /**
     * Evaluates the geometry of a String-array
     * The geometry of a String-array is a int[]-array of
     * the String-array's length containing the length of
     * every element (String).  
     * @param array The String-array.
     * @return The geometry of the String-array.
     */
    public static int[] evaluateGeometryOfStringArray(String[] array)
    {
        int[] geometry = new int[array.length];
        
        for(int i = 0; i < array.length; i++)
        {
            if(array[i] == null)
                geometry[i] = 0;
            else
                geometry[i] = array[i].length();
        }
        
        return geometry;
    }
    
    // chars and chararrays
    
    /**
      * Checks if a char is element of a char-array.
      * @param     c is the character assumed in the array. 
      * @param     chars is the 1D array of chars.
      * @return true, if the char is element of the array. 
      */
    public static boolean containsChar(char[] chars, char c)
    {
            for(int i = 0; i < chars.length; i++) {
                if(c == chars[i])
                    return true;
            }
            return false;    
    }
    
    public static boolean containsLetter(char[] chars, char letter)
    {
        letter = Character.toUpperCase(letter); 
        
        for(char c: chars) {
            if(letter == Character.toUpperCase(c))
                return true;
        }
        return false;    
    }
    
    public static boolean isLetter(char ch, char letter)
    {
        if(Character.toUpperCase(ch) == Character.toUpperCase(letter))
            return true;
        
        return false;
    }

}

