/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tablesemigraphics;

/**
 *
 * @author jan
 */
public class Main
{

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args)
    {
        String one = "This is useful...";
        
        String two = "...to present data on terminal.";
        
        String three = "...to include tables in textfiles.";
        
        String[][] captions = new String[][] {{two,three}};
        
        TableSemigraphic tsg = new TableSemigraphic(captions);
        
        tsg.setHeadline(one);
        
        System.out.println(tsg.toString());
    }
    
}
